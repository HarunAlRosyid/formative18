package com.nexsoft.student;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.boot.test.context.SpringBootTest;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
@SpringBootTest
class StudentApplicationTests {

//	@Test
//	void contextLoads() {
//	}
	

	@Mock
	StudentInterface dataMock;
	
	@InjectMocks
	StudentController StudentImpl;
	
	ArrayList<StudentModel> dataStudent = new ArrayList<StudentModel>();
	
	@Test
	public void testCheckNimTrue() {
		assertTrue(StudentImpl.checkNim("NIM-001"));
	}
	@Test
	public void testCheckNimFalse() {
		assertFalse(StudentImpl.checkNim("NIK-01"));
	}
	
	@Test
	public void testCheckNameTrue() {
		assertTrue(StudentImpl.checkName("Harun"));
	}
	@Test
	public void testCheckNameFalse() {
		assertFalse(StudentImpl.checkName("Harun@@"));
	}
	
	@Test
	public void testCheckAgeTrue() {
		assertTrue(StudentImpl.checkAge(18));
	}
	@Test
	public void testCheckAgeFalse() {
		assertFalse(StudentImpl.checkAge(16));
	}

	@Test
	public void testAddStudentSuccess() {
		dataStudent.add(new StudentModel("NIM-0002","Harun",18));
		when(dataMock.addData()).thenReturn(dataStudent);
		int response = StudentImpl.add("NIM-0002","Harun",18);
		assertEquals(1,response);
	}
	@Test
	public void testAddStudentFail() {
		dataStudent.add(new StudentModel("MH-020","Harun12",16));
		when(dataMock.addData()).thenReturn(dataStudent);
		int response = StudentImpl.add("MH-020","Harun12",16);
		assertEquals(0,response);
	}
	@Test
	public void testAddStudentFailNim() {
		dataStudent.add(new StudentModel("MH-020","Harun",18));
		when(dataMock.addData()).thenReturn(dataStudent);
		assertEquals(0,StudentImpl.add("MH-020","Harun",18));
	}
	@Test
	public void testAddStudentFailName() {
		dataStudent.add(new StudentModel("NIM-0002","Harun1",18));
		when(dataMock.addData()).thenReturn(dataStudent);
		int response = StudentImpl.add("NIM-0002","Harun1",18);
		assertEquals(0,response);
	}
	@Test
	public void testAddStudentFailAge() {
		dataStudent.add(new StudentModel("NIM-0002","Harun",16));
		when(dataMock.addData()).thenReturn(dataStudent);
		int response = StudentImpl.add("NIM-0002","Harun",16);
		assertEquals(0,response);
	}

	@Test
	public void testDeleteStudentSuccess() {
		dataStudent.add(new StudentModel("NIM-0002","Harun",16));
		dataStudent.add(new StudentModel("NIM-0001","Harun",16));
		dataStudent.add(new StudentModel("NIM-0003","Harun",16));
		when(dataMock.addData()).thenReturn(dataStudent);
		int response = StudentImpl.delete("NIM-0002");
		assertEquals(2,response);
	}
	@Test
	public void testDeleteStudentFail() {
		dataStudent.add(new StudentModel("NIM-0002","Harun",16));
		dataStudent.add(new StudentModel("NIM-0001","Harun",16));
		dataStudent.add(new StudentModel("NIM-0003","Harun",16));
		dataStudent.add(new StudentModel("NIM-0005","Harun",16));
		when(dataMock.addData()).thenReturn(dataStudent);
		int response = StudentImpl.delete("NIM-0006");
		assertEquals(4,response);
	}
	
	@Test
	public void testShowStudentFail() {
		dataStudent.add(new StudentModel("NIM-0002","Harun",16));
		dataStudent.add(new StudentModel("NIM-0001","Harun",16));
		dataStudent.add(new StudentModel("NIM-0003","Harun",16));
		dataStudent.add(new StudentModel("NIM-0004","Harun",16));
		when(dataMock.showAll()).thenReturn(dataStudent);
		int response = StudentImpl.allStudent();
		assertEquals(1,response);
	}
}
