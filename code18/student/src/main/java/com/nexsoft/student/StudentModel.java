package com.nexsoft.student;

import java.util.ArrayList;

public class StudentModel {
	private String nim;
	private String name;
	private int age;
	private ArrayList<StudentModel> dataStudent;
		
	public StudentModel(String nim, String name, int age) {
		super();
		this.nim = nim;
		this.name = name;
		this.age = age;
	}

	public StudentModel(ArrayList<StudentModel> dataStudent) {
		this.dataStudent = dataStudent;
	}

	public String getNim() {
		return nim;
	}

	public String getName() {
		return name;
	}

	public int getAge() {
		return age;
	}
	
}

