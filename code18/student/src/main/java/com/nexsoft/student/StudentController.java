package com.nexsoft.student;

import java.util.ArrayList;
import java.util.regex.Pattern;

public class StudentController {
	private StudentModel model;
	private StudentInterface dataService;
	
	ArrayList<StudentModel> dataStudent = new ArrayList<StudentModel>();
	
	public StudentController(StudentModel model) {
		super();
		this.model = model;
	}
	
	public void data(StudentModel data) {
        this.dataStudent.add(data);
	}
	
	public boolean checkNim(String nim) {
		boolean nimFormatCheck = Pattern.compile("NIM-[0-9]{0,10}").matcher(nim).matches();
		if(nimFormatCheck) {
			return true;
		}else {
			return false;
		}
	}
	public boolean checkName(String name) {
		boolean nameFormatCheck = Pattern.compile("[a-zA-Z]{0,50}").matcher(name).matches();
		if(nameFormatCheck) {
			return true;
		}else {
			return false;
		}
	} 
	public boolean checkAge(int age) {
		if(age >= 17) {
			return true;
		}else {
			return false;
		}
	}
	
	public int add(String nim, String name, int age) {
		if(checkNim(nim)&&checkName(name)&&checkAge(age)) {
			ArrayList<StudentModel> addStudent = dataService.retrieveAllData();
			data(new StudentModel(addStudent));
			return dataStudent.size();
		} else {
			return 0;
		}
	}
	
	public int delete(String nim) {
		ArrayList<StudentModel> addStudent= dataService.retrieveAllData();
		data(new StudentModel(addStudent));
			addStudent.removeIf(item -> item.getNim() == nim);
			return  addStudent.size();
			
	}
	
	public int allStudent() {
		ArrayList<StudentModel> addStudent= dataService.retrieveAllData();
		data(new StudentModel(addStudent));
		return addStudent.size();
	}
	
}
